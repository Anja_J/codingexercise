package com.jovic.anja.codingexercise.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.jovic.anja.codingexercise.R;
import com.jovic.anja.codingexercise.database.DatabaseHelper;
import com.jovic.anja.codingexercise.fragments.ItemFragment;
import com.jovic.anja.codingexercise.fragments.ListFragment;
import com.jovic.anja.codingexercise.models.Item;
import com.jovic.anja.codingexercise.rest.ApiClient;
import com.jovic.anja.codingexercise.rest.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "MainActivity";
    public static final int FRAGMENT_LIST = 0;
    public static final int FRAGMENT_ITEM = 1;
    public static final String ID_ITEM = "idItem";

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ListFragment mListFragmnet;
    private ItemFragment mItemFragment;
    private int mCurrentFragment = 0;

    private List<Item> mItems = null;
    private DatabaseHelper mDatabaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mItems = new ArrayList<>();
        mDatabaseHelper = new DatabaseHelper(this);

        changeFragment(mCurrentFragment);
        getItemsForList();

    }

    public void changeFragment(int fragmentId) {

        Fragment fragment = new ListFragment();
        mCurrentFragment = FRAGMENT_LIST;

        switch (fragmentId) {
            case FRAGMENT_LIST:
                if (mListFragmnet == null) {
                    mListFragmnet = new ListFragment();
                }
                fragment = mListFragmnet;
                mCurrentFragment = FRAGMENT_LIST;
                break;

            case FRAGMENT_ITEM:
                if (mItemFragment == null) {
                    mItemFragment = new ItemFragment();
                }
                fragment = mItemFragment;
                mCurrentFragment = FRAGMENT_ITEM;
                break;

        }
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();

    }

    public void setArgumentsForItemFragment(int idItem) {

        Bundle bundle = new Bundle();
        bundle.putInt(ID_ITEM, idItem);

        if (mItemFragment == null) {
            mItemFragment = new ItemFragment();
        }
        mItemFragment.setArguments(bundle);


    }


    @Override
    public void onBackPressed() {
        switch (mCurrentFragment) {
            case FRAGMENT_LIST:

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getResources().getString(R.string.exit_text))
                        .setCancelable(false)
                        .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                exitApp();
                            }
                        })
                        .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

                break;

            case FRAGMENT_ITEM:
                changeFragment(FRAGMENT_LIST);
                break;

        }

    }

    private void exitApp() {
        super.onBackPressed();
    }

    public List<Item> getItemList() {
        return mItems;
    }

    private void getItemsForList() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<List<Item>> call = apiService.getItems();

        mSwipeRefreshLayout.setRefreshing(true);

        call.enqueue(new Callback<List<Item>>() {

            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {

                if (response != null) {
                    mItems = response.body();
                    mDatabaseHelper.insertItems(mItems);

                    Log.d(TAG, "API / Number of received items: " + mItems.size());

                    if (mListFragmnet != null) {
                        mListFragmnet.changeListOfItems(mItems);
                    }
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {
                Log.e(TAG, "API / Error: " + t.toString());

                mItems = mDatabaseHelper.selectAllItems();
                Log.e(TAG, "API / Number of cached items: " + mItems.size());
                if (mItems.size() > 0) {
                    if (mListFragmnet != null) {
                        mListFragmnet.changeListOfItems(mItems);
                    }
                } else {
                    showAlertDialogInternet();
                }
                mSwipeRefreshLayout.setRefreshing(false);

            }

        });
    }

    private void showAlertDialogInternet() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.internet_connection_text))
                .setCancelable(false)
                .setNeutralButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    public void onRefresh() {

        switch (mCurrentFragment) {

            case FRAGMENT_LIST:
                getItemsForList();
                break;

            case FRAGMENT_ITEM:
                mSwipeRefreshLayout.setRefreshing(true);
                if (mItemFragment == null) {
                    mItemFragment = new ItemFragment();
                }
                mItemFragment.updateScreen();

                mSwipeRefreshLayout.setRefreshing(false);
                break;
        }

    }

}
