package com.jovic.anja.codingexercise.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jovic.anja.codingexercise.R;
import com.jovic.anja.codingexercise.models.Item;

import java.util.List;

/**
 * Created by Anja on 21-Jul-16.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ItemViewHolder> {

    private List<Item> mItems;

    public ListAdapter(List<Item> items) {
        mItems = items;

    }

    public void changeList(List<Item> items) {
        mItems = items;
    }

    @Override
    public ListAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
        holder.title.setText(mItems.get(position).getTitle());
        holder.description.setText(mItems.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView description;

        public ItemViewHolder(View v) {
            super(v);
            title = (TextView) v.findViewById(R.id.text_item_title);
            description = (TextView) v.findViewById(R.id.text_item_description);

        }
    }
}
