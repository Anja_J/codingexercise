package com.jovic.anja.codingexercise.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.jovic.anja.codingexercise.models.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anja on 21-Jul-16.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "items.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    private static final String TABLE_NAME = "item";
    private static final String COLUMN_NAME_ID = "id";
    private static final String COLUMN_NAME_TITLE = "title";
    private static final String COLUMN_NAME_DESCRIPTION = "description";
    private static final String COLUMN_NAME_IMAGE_PATH = "image_path";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + COLUMN_NAME_ID + " INTEGER PRIMARY KEY" + COMMA_SEP
                + COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP
                + COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP
                + COLUMN_NAME_IMAGE_PATH + TEXT_TYPE
                + ")";

        db.execSQL(SQL_CREATE_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }


    public void insertItems(List<Item> items) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_NAME, null, null);

        for (Item item : items) {
            ContentValues values = new ContentValues();
            values.put(COLUMN_NAME_TITLE, item.getTitle());
            values.put(COLUMN_NAME_DESCRIPTION, item.getDescription());
            values.put(COLUMN_NAME_IMAGE_PATH, item.getImagePath());
            db.insert(TABLE_NAME, null, values);
        }

        db.close();
    }

    public List<Item> selectAllItems() {
        List<Item> items = new ArrayList<Item>();
        String selectQuery = "SELECT * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Item item = new Item(cursor.getString(1), cursor.getString(2), cursor.getString(3));
                items.add(item);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return items;
    }


}
