package com.jovic.anja.codingexercise.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.jovic.anja.codingexercise.R;
import com.jovic.anja.codingexercise.activities.MainActivity;
import com.jovic.anja.codingexercise.models.Item;

/**
 * Created by Anja on 21-Jul-16.
 */

public class ItemFragment extends Fragment {

    MainActivity mMainActivity;
    Item mItem;
    TextView mTextViewTitle, mTextViewDescription;
    ImageView mImageViewLogo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_item, container, false);
        mTextViewTitle = (TextView) view.findViewById(R.id.text_item_title);
        mTextViewDescription = (TextView) view.findViewById(R.id.text_item_description);
        mImageViewLogo = (ImageView) view.findViewById(R.id.image_item_logo);

        updateScreen();

        return view;
    }

    public void updateScreen() {

        mMainActivity = (MainActivity) getActivity();
        int idItem = getArguments().getInt(MainActivity.ID_ITEM);
        mItem = mMainActivity.getItemList().get(idItem);

        mTextViewTitle.setText(mItem.getTitle());
        mTextViewDescription.setText(mItem.getDescription());

        Glide.with(mMainActivity)
                .load(mItem.getImagePath())
                .error(R.drawable.placeholder_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter()
                .crossFade()
                .into(mImageViewLogo);

    }



}
