package com.jovic.anja.codingexercise.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.jovic.anja.codingexercise.R;
import com.jovic.anja.codingexercise.activities.MainActivity;
import com.jovic.anja.codingexercise.adapters.ListAdapter;
import com.jovic.anja.codingexercise.models.Item;

import java.util.List;

/**
 * Created by Anja on 21-Jul-16.
 */

public class ListFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private MainActivity mMainActivity;
    private ListAdapter mListAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        mMainActivity = (MainActivity) getActivity();

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        if (mMainActivity.getItemList() != null) {
            if (mListAdapter == null) {
                mListAdapter = new ListAdapter(mMainActivity.getItemList());
            }
            mRecyclerView.setAdapter(mListAdapter);
        }

        mRecyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(getActivity().getApplicationContext(), new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                mMainActivity.setArgumentsForItemFragment(position);
                mMainActivity.changeFragment(MainActivity.FRAGMENT_ITEM);
            }
        }));

        return view;
    }

    public void changeListOfItems(List<Item> items) {

        if (mMainActivity == null) {
            mMainActivity = (MainActivity) getActivity();
        }

        if (mListAdapter == null) {
            mListAdapter = new ListAdapter(items);
            mRecyclerView.setAdapter(mListAdapter);
        }

        mListAdapter.changeList(items);
        mListAdapter.notifyDataSetChanged();
    }



    //-------------------------------------------------------------------------------------------------------------
    //RecyclerView Item Click Listener
    //-------------------------------------------------------------------------------------------------------------

    private interface ClickListener {
        void onClick(View view, int position);

    }

    private static class RecyclerViewTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerViewTouchListener(Context context, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

            View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(motionEvent)) {
                clickListener.onClick(child, recyclerView.getChildAdapterPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

}
