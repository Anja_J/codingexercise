package com.jovic.anja.codingexercise.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Anja on 21-Jul-16.
 */
public class Item {

    @SerializedName("title")
    private String mTitle;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("image")
    private String mImagePath;


    public Item(String title, String description, String imagePath) {
        mTitle = title;
        mDescription = description;
        mImagePath = imagePath;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getImagePath() {
        return mImagePath;
    }

    public void setImagePath(String imagePath) {
        mImagePath = imagePath;
    }
}
