package com.jovic.anja.codingexercise.rest;

import com.jovic.anja.codingexercise.models.Item;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Anja on 21-Jul-16.
 */

public interface ApiInterface {

    @GET("danieloskarsson/mobile-coding-exercise/master/items.json")
    Call<List<Item>> getItems();
}
